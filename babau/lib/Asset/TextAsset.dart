class TextAsset {
  static final String loginScreen = "LoginScreen";
  static final String flutterDemo = "Flutter Demo";
  static final String email = "Email";
  static final String password = "Password";
  static final String welcome = "Welcome";
  static final String alert = "Alert";
  static final String userInvalidate = "User invalidate";
  static final String login = "Login";
  static final String close = "Close";
  static final String listView = "ListView";
}