import 'package:flutter/material.dart';

class ImageAsset {
  static final logo = Image.asset('images/logo.png', height: 150.0, fit: BoxFit.contain);
  static final background = new AssetImage("images/bg.jpg");
}