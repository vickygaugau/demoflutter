import 'package:flutter/material.dart';
import 'package:babau/Asset/TextAsset.dart';

class MyDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

  }

  static void showAlertDialog(BuildContext context, String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            new FlatButton(
              child: new Text(TextAsset.close),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

