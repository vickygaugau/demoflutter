import 'package:http/http.dart' as http;

final jsonUrl = "https://api.github.com/orgs/raywenderlich/members";

class Helper {
  static void getDataJson(void Function(http.Response) callback) async {
    http.Response response = await http.get(jsonUrl);
    callback(response);
  }
}