import 'package:flutter_flux/flutter_flux.dart';
import 'package:babau/Flux/Action/UserLoginAction.dart';
import 'package:babau/Flux/Action/MemberAction2.dart';
import 'package:flutter/material.dart';

class LoginUser {
  LoginUser({this.username, this.password});
  String username;
  String password;
}

class LoginUserStore extends Store {
  LoginUserStore() {
    String name = "Vickygaugau";
    String password = "123456";
    _me = new LoginUser(username: name, password: password);

    triggerOnAction(setLoginUserAction, (LoginUser user) {
      debugPrint("LoginUserStore");
      _me.username = user.username;
      _me.password = user.password;
    });
  }

  LoginUser _me;
  LoginUser get me => _me;
}

final StoreToken loginUserStoreToken = new StoreToken(new LoginUserStore());