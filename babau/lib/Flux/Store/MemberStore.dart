import 'package:flutter_flux/flutter_flux.dart';
import 'package:babau/Flux/Action/MemberAction2.dart';
import 'package:babau/Flux/Action/UserLoginAction.dart';
import 'package:babau/Helper/Helper.dart';
import 'dart:convert';
import 'package:flutter/material.dart';

class Member {
  String login;
  String avatarUrl;

  Member({this.login, this.avatarUrl});
}

class MemberStore extends Store {
  MemberStore(){
    _members = <Member>[];

    triggerOnAction(getMember, (_) {
      debugPrint("11111");
      Helper.getDataJson((response) {
        debugPrint("2222");
        _members = [];
        final membersJson = jsonDecode(response.body);
        for (var memberJson in membersJson) {
          final member = Member(login: memberJson["login"], avatarUrl: memberJson["avatar_url"]);
          _members.add(member);
        }
        debugPrint("3333");
        trigger();
      });
    });

//    triggerOnAction(updateMember, (Member member) {
//      members.add(member);
//    });
  }

  List<Member> _members;
  List<Member> get members => _members;
}

final StoreToken memberStoreToken = new StoreToken(new MemberStore());