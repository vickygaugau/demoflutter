import 'package:flutter/material.dart';
import 'WelcomeScreen.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:babau/Flux/Store/UserLoginStore.dart';
import 'package:babau/Flux/Action/UserLoginAction.dart';
import 'package:babau/Flux/Action/MemberAction2.dart';
import 'Asset/ImageAsset.dart';
import 'Asset/TextAsset.dart';
import 'package:babau/Helper/MyDialog.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> with StoreWatcherMixin<LoginScreen> {
  // Propertives
  LoginUserStore loginUserStore;
  TextEditingController usernameEditController = TextEditingController(text: "vu.hung@terralogic.com");
  TextEditingController passwordEditController = TextEditingController(text: "123456");

  @override
  void initState() {
    super.initState();
    loginUserStore = listenToStore(loginUserStoreToken);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: createLayout(),
    );
  }

  // Create Layout
  Widget createLayout() {
    return new Container(
      // background
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: ImageAsset.background,
          fit: BoxFit.cover,
        ),
      ),

      // Children
      child: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            ImageAsset.logo,
            SizedBox(height: 20.0),
            createEmailTextField(),
            SizedBox(height: 20.0),
            createPasswordTextField(),
            SizedBox(height: 20.0),
            createLoginButton(),
          ],
        ),
      )
    );
  }

  Widget createPasswordTextField(){
    return TextField(
      autofocus: false,
      controller: passwordEditController,
      obscureText: true,
      decoration: InputDecoration(
        hintText: TextAsset.password,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
  }

  Widget createEmailTextField(){
    return TextField(
      controller: usernameEditController,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: TextAsset.email,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
  }

  Widget createLoginButton() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        elevation: 5.0,
        color: Colors.lightBlueAccent,
        child: MaterialButton(
          onPressed: () {
            final username = usernameEditController.text;
            final LoginUser newUser = new LoginUser(username: username, password: "123456");

            if (username == "123") {
              MyDialog.showAlertDialog(context, TextAsset.alert, TextAsset.userInvalidate);
            } else {
              setLoginUserAction(newUser);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>
                    WelcomeScreen(title: TextAsset.welcome),
                ),
              );
            }
          },
          child: Text(TextAsset.login, style: TextStyle(color: Colors.white)),
        ),
      ),
    );
  }
}
