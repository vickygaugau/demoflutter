import 'package:flutter/material.dart';
import 'ListViewScreen.dart';
import 'package:babau/Flux/Store/UserLoginStore.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'Asset/TextAsset.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  WelcomeScreenState createState() => new WelcomeScreenState();
}

class WelcomeScreenState extends State<WelcomeScreen> with StoreWatcherMixin<WelcomeScreen> {
  LoginUserStore loginUserStore;

  @override
  void initState() {
    super.initState();
    loginUserStore = listenToStore(loginUserStoreToken);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: createLayout(),
    );
  }

  Widget createLayout() {
    return Container(
      child: Row(
        children: <Widget>[
          IconButton(
            icon: new Icon(Icons.label),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>
                    ListViewScreen(title: TextAsset.listView),
                ),
              );
            },
          ),
          Text(loginUserStore.me.username),
        ],
      )
    );
  }
}
