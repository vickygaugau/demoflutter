import 'package:flutter/material.dart';
import 'LoginScreen.dart';
import 'Asset/TextAsset.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: TextAsset.flutterDemo,
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new LoginScreen(title: TextAsset.loginScreen),
    );
  }
}