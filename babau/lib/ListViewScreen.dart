import 'package:flutter/material.dart';
import 'dart:convert';
import 'Flux/Store/MemberStore.dart';
import 'Flux/Action/MemberAction2.dart';
import 'package:babau/Flux/Action/UserLoginAction.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:babau/Flux/Store/UserLoginStore.dart';

class ListViewScreen extends StatefulWidget {
  ListViewScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  ListViewScreenState createState() => new ListViewScreenState();
}

class ListViewScreenState extends State<ListViewScreen> with StoreWatcherMixin<ListViewScreen> {
  List<Member> _members = [Member(login: "", avatarUrl: "")];
  MemberStore memberStore;
  @override
  void initState() {
    super.initState();
    LoginUserStore loginUserStore;
    loginUserStore = listenToStore(loginUserStoreToken);

    memberStore = listenToStore(memberStoreToken, handleMemberStoreChange);
    debugPrint("0000");
    getMember();
  }

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: new Text(widget.title),
          bottom: TabBar(
            tabs: [
              Tab(text: "Pengding"),
              Tab(text: "Reject"),
              Tab(text: "Approve"),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            createListViewPending(),
            createListViewPending(),
            createListViewPending(),
          ],
        ),
      ),
    );
  }

  Widget createListViewPending() {
    Widget _buildRow(int i) {
      return new Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Stack(
                  alignment: AlignmentDirectional.center,
                  children: <Widget>[
                    CircleAvatar(
                      radius: 20.0,
                      backgroundImage: new NetworkImage(_members[i].avatarUrl),
                    ),
                    Text("H", style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),),
                  ],
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                  width: 150.0,
                  child: RichText(
                    text: TextSpan(
                      text: _members[i].login,
                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                      children: [
                        TextSpan(text: " - MB", style: TextStyle(fontWeight: FontWeight.normal)),
                      ],
                    ),
                    maxLines: 3,
                    softWrap: true,
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                  child: Text("02/09/2018"),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                    alignment: AlignmentDirectional.centerEnd,
                    child: Icon(Icons.star),
                  )
                )
              ],
            ),
            Divider(),
          ],
        )
      );
    }

    return new ListView.builder(
        itemCount: _members.length,
        itemBuilder: (BuildContext context, int position) {
          return _buildRow(position);
        });
  }

  void updateList(String dataJson) {
    final membersJson = jsonDecode(dataJson);
    for (var memberJson in membersJson) {
      final member = Member(login: memberJson["login"], avatarUrl: memberJson["avatar_url"]);
      _members.add(member);
    }
    setState(() {});
  }

  void handleMemberStoreChange(Store store) {
    debugPrint("44444");
    MemberStore ms = store;
    _members = ms.members;
    setState(() {});
  }
}