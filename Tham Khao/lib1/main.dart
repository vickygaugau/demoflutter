import 'package:flutter/material.dart';
import 'define.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'member.dart';

void main() => runApp(new GHFlutter());

var _members = <Member>[];
final _biggerFont = const TextStyle(fontSize: 18.0);

// Test
class GHFlutter extends StatefulWidget {
  @override
  createState() => new GHFlutterState();
}

class GHFlutterState extends State<GHFlutter> {
  @override
  void initState() {
    super.initState();

    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: Strings.appTitle,
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text(Strings.appTitle),
        ),
        body: new ListView.builder(
            itemCount: _members.length,
            itemBuilder: (BuildContext context, int position) {
              return _buildRow(position);
            }),
      ),
      theme: new ThemeData(primaryColor: Colors.red.shade800),
    );
  }

  _loadData() async {
    String dataURL = "https://api.github.com/orgs/raywenderlich/members";
    http.Response response = await http.get(dataURL);
    setState(() {
      final membersJSON = jsonDecode(response.body);
      for (var memberJSON in membersJSON) {
        final member = new Member(memberJSON["login"], memberJSON["avatar_url"]);
        _members.add(member);
      }
    });
  }

  Widget _buildRow(int i) {
    return new Padding(
        padding: const EdgeInsets.all(5.0),
        child: new ListTile(
          title: new Text("${_members[i].login}", style: _biggerFont),
          leading: new CircleAvatar(
              backgroundColor: Colors.green,
              backgroundImage: new NetworkImage(_members[i].avatarUrl)
          ),
        )
    );
  }
}