import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  String textTest = "123";

//  SecondScreen({Key key, @required this.textTest}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Screen"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(textTest),
        ),
      ),
    );
  }
}