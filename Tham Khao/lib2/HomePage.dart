import 'package:flutter/material.dart';
import 'package:flutter_app_2/SecondScreen.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _favoriteCount = 41;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(title: new Text(widget.title)),
        body: new ListView(
          children: <Widget>[
            new Image.asset('images/1.jpg', height: 240.0, fit: BoxFit.cover),
            new Image.asset('images/2.jpg', height: 240.0, fit: BoxFit.cover),
            test(),
            TestButton(),
            TestPositioned(),
            TestCard(),
          ],
        )
    );
  }

  Column buildButtonColumn(IconData icon, String label) {
    Color color = Theme.of(context).primaryColor;

    return new Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        new IconButton(icon: new Icon(icon, color: color), onPressed: _toggleFavorite),
        new Container(
          margin: const EdgeInsets.only(top: 8.0),
          child: new Text(
            _favoriteCount.toString(),
            style: new TextStyle(
              fontSize: 12.0,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }

  Widget TestCard(){
    var card = SizedBox(
      height: 208.0,
      child: Card( // cả 3 thông tin sẽ hiển thị trong 1 card
        child: Column( // hiển thị theo Column
          children: [// và là 1 list children: 3 ListTiles và 1 Divider
            ListTile( // mỗi một thông tin sẽ là một ListTile
              title: Text('1625 Main Street',
                  style: TextStyle(fontWeight: FontWeight.w500)),
              subtitle: Text('My City, CA 99984'),
              leading: Icon(
                Icons.restaurant_menu,
                color: Colors.blue[500],
              ),
            ),
            Divider(),
            ListTile(
              title: Text('(408) 555-1212',
                  style: TextStyle(fontWeight: FontWeight.w500)),
              leading: Icon(
                Icons.contact_phone,
                color: Colors.blue[500],
              ),
            ),
            ListTile(
              title: Text('costa@example.com'),
              leading: Icon(
                Icons.contact_mail,
                color: Colors.blue[500],
              ),
            ),
          ],
        ),
      ),
    );
    return card;
  }

  Widget TestPositioned () {
    return new Container(
        height: 100.0,
        child: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Material(color: Colors.yellowAccent),
            Positioned(
              top: 0.0,
              left: 0.0,
              child: Icon(Icons.star, size: 50.0),
            ),
            Positioned(
              top: 0.0,
              left: 50.0,
              child: Icon(Icons.call, size: 50.0),
            ),
          ],
        )
    );
  }

  Widget TestButton () {
    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          buildButtonColumn(Icons.call, 'CALL'),
          buildButtonColumn(Icons.near_me, 'ROUTE'),
          buildButtonColumn(Icons.share, 'SHARE'),
        ],
      ),
    );
  }

  void _toggleFavorite() {
    print("Click");
    setState(() {
      _favoriteCount++;
    });

    if (_favoriteCount % 5 == 0 ) {
      SecondScreen a = new SecondScreen();
      a.textTest = _favoriteCount.toString();
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) =>
            a
        ),
      );
    }
  }


  Widget test() {
    return new Container(
      padding: const EdgeInsets.all(10.0),
      color: Colors.red,
      height: 100.0,
      child: new Row(
        children: [
          new Expanded(
            flex: 1,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                new Expanded(
                    flex: 1,
                    child: new Container(
                      alignment: AlignmentDirectional.center,
                      color: Colors.green,
                      child: new Text('Oeschinen Lake Campground', style: new TextStyle(fontWeight: FontWeight.bold,),),
                    )
                ),
                new Expanded(
                    flex: 1,
                    child: new Container(
                      color: Colors.yellow,
//                      child: new Text('Kandersteg, Switzerland', style: new TextStyle(color: Colors.grey[500],),),
                    )
                ),
              ],
            ),
          ),
          new Icon(Icons.star, color: Colors.blue),
          new Text('100'),
        ],
      ),
    );
  }
}